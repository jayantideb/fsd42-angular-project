import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { EmpService } from './emp.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'project';
  showHead: boolean = true;
  hrstatus:boolean = true;
  ngOnInit() {
  }
  constructor(private router: Router,private service:EmpService) {
        router.events.forEach((event) => {
        if (this.service.getLoginStatus()===false && this.service.gethrstatus()==false) {
            this.showHead = true;
            this.hrstatus = false;
            console.log("true")
          }
          else if(this.service.getLoginStatus()===true && this.service.gethrstatus()==false){
            this.showHead = false;
            this.hrstatus = false;
          }
          else {
            this.showHead = false;
            this.hrstatus=true;
          }
        });
}}