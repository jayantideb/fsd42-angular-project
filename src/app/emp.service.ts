import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as CryptoJS from 'crypto-js';
import { isPlatformBrowser } from '@angular/common';
import { Observable } from 'rxjs';
function _window(): any {

  // return the global native browser window object

  return window;

}
@Injectable({
  providedIn: 'root'
})
export class EmpService {
  loginStatus: boolean;
  hrStatus:boolean;
  cartItems: any;
  googleloginStatus:boolean;
  constructor(private http: HttpClient,@Inject(PLATFORM_ID) private platformId: object) {
    this.loginStatus = false;
    this.hrStatus = false;
    this.googleloginStatus=false;
    this.cartItems = [];
   }
   setUserLoggedIn() {
    this.loginStatus = true;
  }
  setGoogleLoginStatusIn()
  {
    this.googleloginStatus = true;
  }
  setGoogleLoginStatusOut()
  {
    this.googleloginStatus = false;
  }
  getGoogleLoginStatus():boolean{
    return this.googleloginStatus;
  }
  setUserLoggedOut() {
    this.loginStatus = false;
  }
  sethrstatustrue()
  {
    this.hrStatus = true;
  }
  sethrstatusfalse()
  {
    this.hrStatus = false;
  }
  gethrstatus(): boolean {
    return this.hrStatus;
  }
  getLoginStatus(): boolean {
    return this.loginStatus;
  }
  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }
  empLogin(loginForm: any): any {
    return this.http.get('login/' + loginForm.emailId + "/" + CryptoJS.MD5(loginForm.password).toString()).toPromise();
  }
  getAllProducts(): any {
    return this.http.get('http://localhost:8080/getAllProducts');
  }
  registerCustomer(emp: any): any {
    return this.http.post('registerCustomer', emp);
  }
  getAllCustomers(): any {
    return this.http.get('http://localhost:8080/getAllCustomer');
  }
  registerProduct(emp: any): any {
    return this.http.post('registerProduct', emp);
  }
  getCustomerById(emp:any):any{
    return this.http.get('http://localhost:8080/getCustomerById/'+emp);
  }
  updateCustomer(editEmp: any): any {
    return this.http.put('updateCustomer', editEmp);
  }
  deleteCustomer(empId: any) {
    return this.http.delete('deleteCustomer/' + empId);
  }
  updateProduct(editEmp: any): any {
    return this.http.put('updateProduct', editEmp);
  }
  deleteProduct(empId: any) {
    return this.http.delete('deleteProduct/' + empId);
  }
  addToCart(product: any) {
    this.cartItems.push(product);
  }
  updateStatus(emp:any)
  {
    return this.http.put('updateStatus', emp);
  }
  sendemail(toemail:string,subject:string,body:string)
  {
    return this.http.get('sendmail/'+toemail+"/"+subject+"/"+body);
  }
  get nativeWindow(): any {
    if (isPlatformBrowser(this.platformId)) {
      return _window();
    }
  }
  payment(total:number):any
  { console.log(total)
    return this.http.get('payment/'+total)
}
}