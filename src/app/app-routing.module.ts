import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { ShowallcustomersComponent } from './showallcustomers/showallcustomers.component';
import { UserComponent } from './user/user.component';
import { User2Component } from './user2/user2.component';
import { AboutComponent } from './about/about.component';
import { ShowallproductsComponent } from './showallproducts/showallproducts.component';
import { AddproductsComponent } from './addproducts/addproducts.component';
import { ShowcustomerbyidComponent } from './showcustomerbyid/showcustomerbyid.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { AppComponent } from './app.component';
import { EmailComponent } from './email/email.component';
import { PaymentComponent } from './payment/payment.component';
import { About2Component } from './about2/about2.component';
import { About3Component } from './about3/about3.component';
import { About4Component } from './about4/about4.component';
import { About5Component } from './about5/about5.component';
import { About6Component } from './about6/about6.component';
import { VideoComponent } from './video/video.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'login', component:LoginComponent},
  {path:'register', component:RegisterComponent},
  {path:'logout',component:LogoutComponent},
  {path:'showcustomers',component:ShowallcustomersComponent},
  {path:'user',component:UserComponent},
  {path:'user2',component:User2Component},
  {path:'about',component:AboutComponent},
  {path:'showproducts',component:ShowallproductsComponent},
  {path:'addproducts',component:AddproductsComponent},
  {path:'showcustomersbyid',component:ShowcustomerbyidComponent},
  {path:'Cart',component:CartComponent},
  {path:'app',component:AppComponent},
  {path:'email',component:EmailComponent},
  {path:'payment/:total',component:PaymentComponent},
  {path:'about2',component:About2Component},
  {path:'about3',component:About3Component},
  {path:'about4',component:About4Component},
  {path:'about5',component:About5Component},
  {path:'about6',component:About6Component},
  {path:'video',component:VideoComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }