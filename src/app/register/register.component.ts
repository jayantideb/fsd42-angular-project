import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { NgxCaptchaModule } from 'ngx-captcha';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as CryptoJS from 'crypto-js';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  countries: any;
  employees: any;
  products: any;
  customer: any;
  constructor(private service: EmpService) {

    this.customer = {customerId:'', customerName:'', dob:'',address:'',city:'',country:'', emailId:'', password:'',phonenumber:'',status:'active'};

  }

  ngOnInit(){
    this.service.getAllCountries().subscribe((countriesData: any) => {
      this.countries = countriesData;
      console.log(countriesData);

    });

  }

  register(regForm: any) {
    console.log(regForm);

    this.customer.customerId = regForm.customerId;
    this.customer.customerName = regForm.customerName;
    this.customer.dob = regForm.dob;
    this.customer.address = regForm.address;
    this.customer.country = regForm.country;
    this.customer.city = regForm.city;
    this.customer.emailId = regForm.emailId;
    this.customer.password = CryptoJS.MD5(regForm.password).toString();
    this.customer.phonenumber = regForm.phonenumber;
    console.log(this.customer);

    this.service.registerCustomer(this.customer).subscribe((result: any) => {
      alert('Register successfully');
    });
  }
    checkpassword(password:any):boolean
       {
        const number=['0','1','2','3','4','5','6','7','8','9'];
        const special=['*','!','@','#','$'];
        let u=0,l=0,n=0,s=0;
        for(let i=0;i<password.length;i++)
        {
          if(password.charAt(i).toUpperCase()==password.charAt(i))
             u=u+1;
          else if(password.charAt(i).toLowerCase()==password.charAt(i))
             l=l+1;
          for(let j=0;j<number.length;j++)
          {if(number[j]==password.charAt(i))
           n=n+1;}
           for(let j=0;j<special.length;j++)
           {if(special[j]==password.charAt(i))
            s=s+1;}
        }
        if(u>0&&l>0&&n>0&&s>0)
        { console.log(password);
          console.log(u+" "+l+" "+n+"  "+s);
          return true;}
        else
        { console.log(password);
          console.log(u+" "+l+" "+n+"  "+s);
          return false;}
       }
       checknumber(number:any):boolean{
        if(number.charAt(0)=='9'||number.charAt(0)=='8'||number.charAt(0)=='7'||number.charAt(0)=='6')
        return true;
        else
        return false;
      }
      checkemailId(email:any):boolean{
        let a=0;
        for(let i=0;i<email.length;i++)
        {
          if(email.charAt(i)=='@')
          a=a+1;
        }
        if(a==1&& email.charAt(email.length-1)=='m'&& email.charAt(email.length-2)=='o'&&email.charAt(email.length-3)=='c'&&email.charAt(email.length-4)=='.')
        return true;
        else
        return false;
      }
      checkconfirmpassword(password:any,confirmpasssword:any):boolean{
        if(password===confirmpasssword)
        return true;
        else
        return false;
}


}