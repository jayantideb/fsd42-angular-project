import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-showcustomerbyid',
  templateUrl: './showcustomerbyid.component.html',
  styleUrls: ['./showcustomerbyid.component.css']
})
export class ShowcustomerbyidComponent implements OnInit{
  cust: any;
  customer: any;
  showHead:boolean =true;
  hrstatus:boolean =true;
  constructor(private service: EmpService,private router:Router) {
    router.events.forEach((event) => {
      if (this.service.getLoginStatus()===false && this.service.gethrstatus()==false) {
          this.showHead = true;
          this.hrstatus = false;
          console.log("true")
        }
        else if(this.service.getLoginStatus()===true && this.service.gethrstatus()==false){
          this.showHead = false;
          this.hrstatus = false;
        }
        else {
          this.showHead = false;
          this.hrstatus=true;
        }
      });
  }
  ngOnInit() {

  }
  getCustomer(empForm: any) {
    this.service.getCustomerById(empForm.empId).subscribe((empData: any) => {
      this.cust = empData;
    });
  }

}