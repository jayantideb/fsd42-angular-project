import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class AuthServiceGuard  {
  isLoggedIn = false;

  login(): void {
    // Perform login logic here
    this.isLoggedIn = true;
  }

  logout(): void {
    // Perform logout logic here
    this.isLoggedIn = false;
  }

}
