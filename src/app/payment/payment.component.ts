import { HttpClient} from '@angular/common/http';
import { Component, OnInit, HostListener, Output } from '@angular/core';
import { EmpService } from '../emp.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
declare var Razorpay: any;
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit{
  title = 'demo';
  paymentId: string="";
  error: string="";
  form: any = {};
  total:any;
  order_id:any;
   constructor(private http: HttpClient,
    private service:EmpService,private route:ActivatedRoute) {
    this.total=this.route.snapshot.paramMap.get('total');
  }
  ngOnInit() {
    this.total=this.route.snapshot.paramMap.get('total');
    const RazorPayOptions={
      description:"sample razor",
      currency:"INR",
      amount:this.total*100,
      key:'rzp_test_ouANwosKHBxdrp',
      callback_url:"http://localhost:4200",
      prefill:{

      },
      theme:
      {
        color:"blue",
      },
      modal:{
        ondismiss: ()=>{
          console.log('dismiss');
        }
      }
    }
    const successCallback=(paymentid:any)=>{
      console.log(paymentid);
      window.location.href = "http://localhost:4200/";
    }
    const failureCallback=(e:any)=>{
      console.log(e);
      window.location.href = "http://localhost:4200/";
    }
    Razorpay.open(RazorPayOptions,successCallback,failureCallback)
}

}