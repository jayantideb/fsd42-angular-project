import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AbstractControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';

import { ShowallproductsComponent } from './showallproducts/showallproducts.component';
import { ShowallcustomersComponent } from './showallcustomers/showallcustomers.component';
import { UserComponent } from './user/user.component';
import { User2Component } from './user2/user2.component';
import { AboutComponent } from './about/about.component';
import { AddproductsComponent } from './addproducts/addproducts.component';
import { ShowcustomerbyidComponent } from './showcustomerbyid/showcustomerbyid.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { PaymentComponent } from './payment/payment.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { RouterModule } from '@angular/router';
import { SocialLoginModule, SocialAuthServiceConfig } from '@abacritt/angularx-social-login';
import {GoogleLoginProvider,GoogleSigninButtonModule} from '@abacritt/angularx-social-login';
import {GoogleAuthService} from './google-auth.service';
import { EmailComponent } from './email/email.component';
import { EmailService } from './email.service';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';
import { About2Component } from './about2/about2.component';
import { About3Component } from './about3/about3.component';
import { About4Component } from './about4/about4.component';
import { About5Component } from './about5/about5.component';
import { About6Component } from './about6/about6.component';
import { VideoComponent } from './video/video.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    LogoutComponent,
    ShowallproductsComponent,
    ShowallcustomersComponent,
    UserComponent,
    User2Component,
    AboutComponent,
    AddproductsComponent,
    ShowcustomerbyidComponent,
    HomeComponent,
    CartComponent,
    PaymentComponent,
    EmailComponent,
    FooterComponent,
    About2Component,
    About3Component,
    About4Component,
    About5Component,
    About6Component,
    VideoComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    SocialLoginModule,
    GoogleSigninButtonModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers:[
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '981690278553-lfkohpvvlhkk09cc426ljef70d03ub7q.apps.googleusercontent.com'
            )
          }
        ],
        onError: (err) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }