import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';
declare var jQuery: any;
@Component({
  selector: 'app-showallcustomers',
  templateUrl: './showallcustomers.component.html',
  styleUrls: ['./showallcustomers.component.css']
})
export class ShowallcustomersComponent implements OnInit{
  customers:any;
  editEmp: any;
  countries:any;
  constructor(private service: EmpService,private router:Router)
  {
    this.editEmp = {customerId:'', customerName:'', dob:'', address:'', city:'', country:'', emailId:'', password:'',phonenumber:'',status:''};
  }
  ngOnInit() {
    this.service.getAllCustomers().subscribe((data: any) => {
      this.customers = data;
      console.log(data);
    });
    this.service.getAllCountries().subscribe((data: any) => {this.countries = data;});
  }
  editEmployee(emp: any) {
    this.editEmp = emp;
    console.log(emp);

    jQuery('#editEmployee').modal('show');
  }
    updateEmp() {
    console.log(this.editEmp);
    this.service.updateCustomer(this.editEmp).subscribe((data: any) => {console.log(data);});
  }
  UpdateStatus(emp:any)
  {
    this.service.updateStatus(emp).subscribe((data: any) => {
    console.log(data);});
    this.router.navigate(['showcustomers'])
  }
}