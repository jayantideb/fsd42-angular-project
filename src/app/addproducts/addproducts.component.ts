import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-addproducts',
  templateUrl: './addproducts.component.html',
  styleUrls: ['./addproducts.component.css']
})
export class AddproductsComponent implements OnInit{
  products:any;
  showHead:boolean=true;
  hrstatus:boolean=true;
  selectedFile: any;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string="";
  imageName: any;
  httpClient: any;
  url=""
  constructor(private router: Router,private service:EmpService) {
    this.products = {productId:'', productName:'',price:'',imagePath:'', description:''};
      router.events.forEach((event) => {
        if (this.service.getLoginStatus()===false && this.service.gethrstatus()==false) {
            this.showHead = true;
            this.hrstatus = false;
            console.log("true")
          }
          else if(this.service.getLoginStatus()===true && this.service.gethrstatus()==false){
            this.showHead = false;
            this.hrstatus = false;
          }
          else {
            this.showHead = false;
            this.hrstatus=true;
          }
        });
  }
  ngOnInit(){

  }
   getshowheadstatus():boolean
   {
    return this.showHead;
   }
   getshowhrstatus():boolean
   {
    return this.hrstatus;
   }
  registerProduct(regForm: any) {
    console.log(regForm);

    this.products.productId = regForm.productId;
    this.products.productName = regForm.productName;
    this.products.price = regForm.price;
    this.products.imagePath=this.url;
    this.products.description = regForm.description;
    console.log(this.products);

    this.service.registerProduct(this.products).subscribe((result: any) => {
      alert('Register successfully');
    });
}
onSelect(event:any) {
  let fileType = event.target.files[0].type;
  if (fileType.match(/image\/*/)) {
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event: any) => {
      this.url = event.target.result;
    };
  } else {
    window.alert('Please select correct image format');
}
}
}