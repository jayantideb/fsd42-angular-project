import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user2',
  templateUrl: './user2.component.html',
  styleUrls: ['./user2.component.css']
})
export class User2Component implements OnInit {
  cartItems: any;
  constructor(private router: Router,private service:EmpService)
  {

  }
  ngOnInit(){
    this.cartItems = this.service.cartItems;
  }

}