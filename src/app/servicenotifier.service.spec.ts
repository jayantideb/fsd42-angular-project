import { TestBed } from '@angular/core/testing';

import { ServicenotifierService } from './servicenotifier.service';

describe('ServicenotifierService', () => {
  let service: ServicenotifierService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicenotifierService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
