import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';
import { GoogleLoginProvider, SocialAuthService,SocialLoginModule,GoogleSigninButtonModule, } from '@abacritt/angularx-social-login';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})

export class LogoutComponent implements OnInit {
  constructor(private service: EmpService, private router: Router) {
  }
  ngOnInit(){
    this.service.setUserLoggedOut();
    this.service.sethrstatusfalse();
    this.service.setGoogleLoginStatusOut();
    window.location.href = "http://localhost:4200/";
    alert("Logged out successful")

}

}