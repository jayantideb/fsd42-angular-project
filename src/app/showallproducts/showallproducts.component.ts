import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';
declare var jQuery: any;
@Component({
  selector: 'app-showallproducts',
  templateUrl: './showallproducts.component.html',
  styleUrls: ['./showallproducts.component.css']
})
export class ShowallproductsComponent implements OnInit {
  products: any;
  showHead:boolean=true;
  hrstatus:boolean=true;
  editEmp:any;
  cartItems: any;
  constructor(private router: Router,private service:EmpService) {
    this.editEmp = {productId:'', productName:'', price:'', imagePath:'', description:''};
      router.events.forEach((event) => {
        if (this.service.getLoginStatus()===false && this.service.gethrstatus()==false) {
            this.showHead = true;
            this.hrstatus = false;
            console.log("true")
          }
          else if(this.service.getLoginStatus()===true && this.service.gethrstatus()==false){
            this.showHead = false;
            this.hrstatus = false;
          }
          else {
            this.showHead = false;
            this.hrstatus=true;
          }
        });
  }
  ngOnInit(){
    this.service.getAllProducts().subscribe((data: any) => {
      this.products = data;
      console.log(data);
  });}
   getshowheadstatus():boolean
   {
    return this.showHead;
   }
   getshowhrstatus():boolean
   {
    return this.hrstatus;
   }
   editEmployee(emp: any) {
    this.editEmp = emp;
    console.log(emp);

    jQuery('#editEmployee').modal('show');
  }
    updateEmp() {
    console.log(this.editEmp);
    this.service.updateProduct(this.editEmp).subscribe((data: any) => {console.log(data);});
  }
  deleteEmployee(emp: any) {
    this.service.deleteProduct(emp.productId).subscribe((data: any) => {console.log(data);});

    const i = this.products.findIndex((product: any) => {
      return product.customerId === emp.productId;
    });
    this.products.splice(i, 1);

  }
  addToCart(product: any) {
    this.service.addToCart(product);

  }
}