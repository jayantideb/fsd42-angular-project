import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit{
  cartItems: any;
  cartData: any;
  total: number;
  showHead:boolean =true;
  hrstatus:boolean =true;

  constructor(private service: EmpService,private router:Router) {
    this.cartItems = [];
    this.total = 0;
      router.events.forEach((event) => {
        if (this.service.getLoginStatus()===false && this.service.gethrstatus()==false) {
            this.showHead = true;
            this.hrstatus = false;
            console.log("true")
          }
          else if(this.service.getLoginStatus()===true && this.service.gethrstatus()==false){
            this.showHead = false;
            this.hrstatus = false;
          }
          else {
            this.showHead = false;
            this.hrstatus=true;
          }
        });
  }

  ngOnInit() {

    this.cartItems = this.service.cartItems;
    this.cartItems.forEach((product: any) => {
      this.total = this.total + product.price;
    });
  }

  payment()
  { html2canvas(document.getElementById("content")!).then(canvas => {

    const contentDataURL = canvas.toDataURL('image/png')
    let pdf = new jsPDF('p', 'mm', 'a4');
    pdf.text("Invoice",50,10);
    var width = pdf.internal.pageSize.getWidth();
    var height = canvas.height * width / canvas.width;
    pdf.addImage(contentDataURL, 'PNG', 0, 25, width, height)
    pdf.save('output.pdf'); // Generated PDF
    pdf.text("Delivery expected to be done within 7 business days subject to payment",130,5);
    });
    this.router.navigate(['payment/',this.total]);
  }
}