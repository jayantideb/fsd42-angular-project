import { ComponentFixture, TestBed } from '@angular/core/testing';

import { About6Component } from './about6.component';

describe('About6Component', () => {
  let component: About6Component;
  let fixture: ComponentFixture<About6Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [About6Component]
    });
    fixture = TestBed.createComponent(About6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
