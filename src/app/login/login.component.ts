import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GoogleLoginProvider, SocialAuthService, SocialLoginModule, GoogleSigninButtonModule } from '@abacritt/angularx-social-login';
import * as CryptoJS from 'crypto-js';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  protected aFormGroup: FormGroup;
  emailId: any;
  password: any;
  employee: any;
  user: any;
  loggedIn: boolean = this.service.getGoogleLoginStatus();
  constructor(private service: EmpService, private router: Router, private formBuilder: FormBuilder, private authService: SocialAuthService, private toastr: ToastrService) {
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });
  }
  emailSubject: string = "Caution";
  emailBody: string = "Some one has logged in using your account. If not you please check your password";

  ngOnInit() {
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });
    if (this.service.getGoogleLoginStatus() == false) {
      this.authService.authState.subscribe((user) => {
        this.user = user;
        this.loggedIn = (user != null);
        console.log(this.user + " " + "loggedIn=" + this.loggedIn);
        this.service.setGoogleLoginStatusIn();
        this.service.setUserLoggedIn();
        this.service.sethrstatusfalse();
        this.toastr.success('Logged in successfully');
        this.router.navigate(['user2']);
        console.log(this.service.getLoginStatus())
        this.service.sendemail(this.user.email, this.emailSubject, this.emailBody).subscribe((data) => {
          console.log("sent successfully")
        }
        );

      });
    }
  }
  sitekey: string = "6LeAajImAAAAAF0szcIqS92U5J6YcmzE-whA3kIR";
  refreshToken(): void {
    this.authService.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
  }
  async loginSubmit(loginForm: any) {
    console.log(loginForm);

    if (loginForm.emailId === 'ADMIN' && loginForm.password === 'ADMIN') {
      alert("Welcome to ADMIN login Page")
      this.service.setUserLoggedIn();
      this.service.sethrstatustrue();
      this.toastr.success('Logged in successfully');
      this.router.navigate(['user']);
    }

    else {

      await this.service.empLogin(loginForm).then((emp: any) => {
        this.employee = emp;
        console.log(emp);
      });

      if (this.employee != null) {
        this.service.setUserLoggedIn();
        this.service.sethrstatusfalse();
        this.toastr.success('Logged in successfully');
        this.router.navigate(['user2']);
        this.service.sendemail(this.employee.emailId, this.emailSubject, this.emailBody).subscribe((data) => {
          console.log("sent successfully")
        }
        );


      } else {
        this.toastr.error('Invalid logged in');
      }

    }
  }

}